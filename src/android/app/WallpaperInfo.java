package android.app;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ServiceInfo;
//import android.content.pm.PackageManager;
//import android.content.pm.ResolveInfo;
//import android.content.pm.ServiceInfo;
//import android.content.res.Resources.NotFoundException;
//import android.content.res.TypedArray;
//import android.content.res.XmlResourceParser;
//import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
//import android.service.wallpaper.WallpaperService;
//import android.util.AttributeSet;
//import android.util.Printer;
//import android.util.Xml;

import java.io.IOException;

/**
 * This class is used to specify meta information of a wallpaper service.
 */
public final class WallpaperInfo implements Parcelable {

    /**
     * Constructor.
     * 
     * @param context The Context in which we are parsing the wallpaper.
     * @param service The ResolveInfo returned from the package manager about
     * this wallpaper's component.
     */
//    public WallpaperInfo(Context context, ResolveInfo service)
//            throws XmlPullParserException, IOException {
//    }

    /**
     * Return the .apk package that implements this wallpaper.
     */
    public String getPackageName() {
        return null;
    }
    
    /**
     * Return the class name of the service component that implements
     * this wallpaper.
     */
    public String getServiceName() {
        return null;
    }

    /**
     * Return the raw information about the Service implementing this
     * wallpaper.  Do not modify the returned object.
     */
    public ServiceInfo getServiceInfo() {
        return null;
    }

    /**
     * Return the component of the service that implements this wallpaper.
     */
    public ComponentName getComponent() {
        return null;
    }
    
    /**
     * Load the user-displayed label for this wallpaper.
     * 
     * @param pm Supply a PackageManager used to load the wallpaper's
     * resources.
     */
//    public CharSequence loadLabel(PackageManager pm) {
//        return null;
//    }
    
    /**
     * Load the user-displayed icon for this wallpaper.
     * 
     * @param pm Supply a PackageManager used to load the wallpaper's
     * resources.
     */
//    public Drawable loadIcon(PackageManager pm) {
//        return null;
//    }
    
    /**
     * Load the thumbnail image for this wallpaper.
     * 
     * @param pm Supply a PackageManager used to load the wallpaper's
     * resources.
     */
//    public Drawable loadThumbnail(PackageManager pm) {
//        return null;
//    }

    /**
     * Return a string indicating the author(s) of this wallpaper.
     */
//    public CharSequence loadAuthor(PackageManager pm) throws NotFoundException {
//        return null;    }

    /**
     * Return a brief summary of this wallpaper's behavior.
     */
//    public CharSequence loadDescription(PackageManager pm) throws NotFoundException {
//        return null;
//    }
    
    /**
     * Return the class name of an activity that provides a settings UI for
     * the wallpaper.  You can launch this activity be starting it with
     * an {@link android.content.Intent} whose action is MAIN and with an
     * explicit {@link android.content.ComponentName}
     * composed of {@link #getPackageName} and the class name returned here.
     * 
     * <p>A null will be returned if there is no settings activity associated
     * with the wallpaper.
     */
    public String getSettingsActivity() {
        return null;
    }
    
//    public void dump(Printer pw, String prefix) {
//    }
    
    @Override
    public String toString() {
        return null;
    }

    /**
     * Used to package this object into a {@link Parcel}.
     * 
     * @param dest The {@link Parcel} to be written.
     * @param flags The flags used for parceling.
     */
    public void writeToParcel(Parcel dest, int flags) {
    }


    public int describeContents() {
        return 0;
    }


    /**
     * Used to make this class parcelable.
     */
    public static final Parcelable.Creator<WallpaperInfo> CREATOR = new Parcelable.Creator<WallpaperInfo>() {
        public WallpaperInfo createFromParcel(Parcel source) {
            return null;
        }

        public WallpaperInfo[] newArray(int size) {
            return null;
        }
    };

}
