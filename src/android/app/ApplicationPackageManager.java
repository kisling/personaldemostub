/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.ContainerEncryptionParams;
import android.content.pm.FeatureInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageInstallObserver;
import android.content.pm.IPackageManager;
import android.content.pm.IPackageMoveObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.InstrumentationInfo;
import android.content.pm.ManifestDigest;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.VerificationParams;
import android.content.pm.VerifierDeviceIdentity;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*package*/
final class ApplicationPackageManager extends PackageManager {
    private static final String TAG = "ApplicationPackageManager";
    private final static boolean DEBUG = false;
    private final static boolean DEBUG_ICONS = false;

    @Override
    public PackageInfo getPackageInfo(final String packageName, final int flags)
            throws NameNotFoundException {
        // try {
        // PackageInfo pi = mPM.getPackageInfo(packageName, flags, mContext.getUserId());
        // if (pi != null) {
        // return pi;
        // }
        // } catch (RemoteException e) {
        // throw new RuntimeException("Package manager has died", e);
        // }

        throw new NameNotFoundException(packageName);
    }

    @Override
    public String[] currentToCanonicalPackageNames(final String[] names) {
        try {
            return mPM.currentToCanonicalPackageNames(names);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public String[] canonicalToCurrentPackageNames(final String[] names) {
        try {
            return mPM.canonicalToCurrentPackageNames(names);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public Intent getLaunchIntentForPackage(final String packageName) {
        // First see if the package has an INFO activity; the existence of
        // such an activity is implied to be the desired front-door for the
        // overall package (such as if it has multiple launcher entries).
        Intent intentToResolve = new Intent(Intent.ACTION_MAIN);
        intentToResolve.addCategory(Intent.CATEGORY_INFO);
        intentToResolve.setPackage(packageName);
        List<ResolveInfo> ris = queryIntentActivities(intentToResolve, 0);

        // Otherwise, try to find a main launcher activity.
        if (ris == null || ris.size() <= 0) {
            // reuse the intent instance
            intentToResolve.removeCategory(Intent.CATEGORY_INFO);
            intentToResolve.addCategory(Intent.CATEGORY_LAUNCHER);
            intentToResolve.setPackage(packageName);
            ris = queryIntentActivities(intentToResolve, 0);
        }
        if (ris == null || ris.size() <= 0) {
            return null;
        }
        Intent intent = new Intent(intentToResolve);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName(ris.get(0).activityInfo.packageName,
                ris.get(0).activityInfo.name);
        return intent;
    }

    @Override
    public int[] getPackageGids(final String packageName)
            throws NameNotFoundException {
        try {
            int[] gids = mPM.getPackageGids(packageName);
            if (gids == null || gids.length > 0) {
                return gids;
            }
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }

        throw new NameNotFoundException(packageName);
    }

    @Override
    public PermissionInfo getPermissionInfo(final String name, final int flags)
            throws NameNotFoundException {
        try {
            PermissionInfo pi = mPM.getPermissionInfo(name, flags);
            if (pi != null) {
                return pi;
            }
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }

        throw new NameNotFoundException(name);
    }

    @Override
    public List<PermissionInfo> queryPermissionsByGroup(final String group, final int flags)
            throws NameNotFoundException {
        try {
            List<PermissionInfo> pi = mPM.queryPermissionsByGroup(group, flags);
            if (pi != null) {
                return pi;
            }
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }

        throw new NameNotFoundException(group);
    }

    @Override
    public PermissionGroupInfo getPermissionGroupInfo(final String name,
            final int flags) throws NameNotFoundException {
        try {
            PermissionGroupInfo pgi = mPM.getPermissionGroupInfo(name, flags);
            if (pgi != null) {
                return pgi;
            }
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }

        throw new NameNotFoundException(name);
    }

    @Override
    public List<PermissionGroupInfo> getAllPermissionGroups(final int flags) {
        try {
            return mPM.getAllPermissionGroups(flags);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }




    @Override
    public String[] getSystemSharedLibraryNames() {
        try {
            return mPM.getSystemSharedLibraryNames();
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public FeatureInfo[] getSystemAvailableFeatures() {
        try {
            return mPM.getSystemAvailableFeatures();
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public boolean hasSystemFeature(final String name) {
        try {
            return mPM.hasSystemFeature(name);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public int checkPermission(final String permName, final String pkgName) {
        try {
            return mPM.checkPermission(permName, pkgName);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public boolean addPermission(final PermissionInfo info) {
        try {
            return mPM.addPermission(info);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public boolean addPermissionAsync(final PermissionInfo info) {
        try {
            return mPM.addPermissionAsync(info);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public void removePermission(final String name) {
        try {
            mPM.removePermission(name);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }


    @Override
    public int checkSignatures(final String pkg1, final String pkg2) {
        try {
            return mPM.checkSignatures(pkg1, pkg2);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public int checkSignatures(final int uid1, final int uid2) {
        try {
            return mPM.checkUidSignatures(uid1, uid2);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public String[] getPackagesForUid(final int uid) {
        try {
            return mPM.getPackagesForUid(uid);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public String getNameForUid(final int uid) {
        try {
            return mPM.getNameForUid(uid);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public int getUidForSharedUser(final String sharedUserName)
            throws NameNotFoundException {
        try {
            int uid = mPM.getUidForSharedUser(sharedUserName);
            if(uid != -1) {
                return uid;
            }
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
        throw new NameNotFoundException("No shared userid for user:"+sharedUserName);
    }








    @Override
    public List<ProviderInfo> queryContentProviders(final String processName,
            final int uid, final int flags) {
        try {
            return mPM.queryContentProviders(processName, uid, flags);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    @Override
    public InstrumentationInfo getInstrumentationInfo(
            final ComponentName className, final int flags)
                    throws NameNotFoundException {
        try {
            InstrumentationInfo ii = mPM.getInstrumentationInfo(
                    className, flags);
            if (ii != null) {
                return ii;
            }
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }

        throw new NameNotFoundException(className.toString());
    }

    @Override
    public List<InstrumentationInfo> queryInstrumentation(
            final String targetPackage, final int flags) {
        try {
            return mPM.queryInstrumentation(targetPackage, flags);
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }


    @Override public Drawable getActivityIcon(final ComponentName activityName)
            throws NameNotFoundException {
        return getActivityInfo(activityName, 0).loadIcon(this);
    }

    @Override public Drawable getActivityIcon(final Intent intent)
            throws NameNotFoundException {
        if (intent.getComponent() != null) {
            return getActivityIcon(intent.getComponent());
        }

        ResolveInfo info = resolveActivity(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (info != null) {
            return info.activityInfo.loadIcon(this);
        }

        throw new NameNotFoundException(intent.toUri(0));
    }


    @Override public Drawable getApplicationIcon(final ApplicationInfo info) {
        return info.loadIcon(this);
    }

    @Override public Drawable getApplicationIcon(final String packageName)
            throws NameNotFoundException {
        return getApplicationIcon(getApplicationInfo(packageName, 0));
    }

    @Override
    public Drawable getActivityLogo(final ComponentName activityName)
            throws NameNotFoundException {
        return getActivityInfo(activityName, 0).loadLogo(this);
    }

    @Override
    public Drawable getActivityLogo(final Intent intent)
            throws NameNotFoundException {
        if (intent.getComponent() != null) {
            return getActivityLogo(intent.getComponent());
        }

        ResolveInfo info = resolveActivity(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (info != null) {
            return info.activityInfo.loadLogo(this);
        }

        throw new NameNotFoundException(intent.toUri(0));
    }

    @Override
    public Drawable getApplicationLogo(final ApplicationInfo info) {
        return info.loadLogo(this);
    }

    @Override
    public Drawable getApplicationLogo(final String packageName)
            throws NameNotFoundException {
        return getApplicationLogo(getApplicationInfo(packageName, 0));
    }

    @Override public Resources getResourcesForActivity(
            final ComponentName activityName) throws NameNotFoundException {
        return getResourcesForApplication(
                getActivityInfo(activityName, 0).applicationInfo);
    }


    @Override public Resources getResourcesForApplication(
            final String appPackageName) throws NameNotFoundException {
        return getResourcesForApplication(
                getApplicationInfo(appPackageName, 0));
    }


    int mCachedSafeMode = -1;
    @Override public boolean isSafeMode() {
        try {
            if (mCachedSafeMode < 0) {
                mCachedSafeMode = mPM.isSafeMode() ? 1 : 0;
            }
            return mCachedSafeMode != 0;
        } catch (RemoteException e) {
            throw new RuntimeException("Package manager has died", e);
        }
    }

    static void configurationChanged() {
        synchronized (sSync) {
            sIconCache.clear();
            sStringCache.clear();
        }
    }

    // ApplicationPackageManager(final ContextImpl context,
    // final IPackageManager pm) {
    // mContext = context;
    // mPM = pm;
    // }

    private Drawable getCachedIcon(final ResourceName name) {
        synchronized (sSync) {
            WeakReference<Drawable.ConstantState> wr = sIconCache.get(name);
            if (DEBUG_ICONS) {
                Log.v(TAG, "Get cached weak drawable ref for "
                        + name + ": " + wr);
            }
            if (wr != null) {   // we have the activity
                Drawable.ConstantState state = wr.get();
                if (state != null) {
                    if (DEBUG_ICONS) {
                        Log.v(TAG, "Get cached drawable state for " + name + ": " + state);
                    }
                    // Note: It's okay here to not use the newDrawable(Resources) variant
                    //       of the API. The ConstantState comes from a drawable that was
                    //       originally created by passing the proper app Resources instance
                    //       which means the state should already contain the proper
                    //       resources specific information (like density.) See
                    //       BitmapDrawable.BitmapState for instance.
                    return state.newDrawable();
                }
                // our entry has been purged
                sIconCache.remove(name);
            }
        }
        return null;
    }

    private void putCachedIcon(final ResourceName name, final Drawable dr) {
        synchronized (sSync) {
            sIconCache.put(name, new WeakReference<Drawable.ConstantState>(dr.getConstantState()));
            if (DEBUG_ICONS) {
                Log.v(TAG, "Added cached drawable state for " + name + ": " + dr);
            }
        }
    }


    private static final class ResourceName {
        final String packageName;
        final int iconId;

        ResourceName(final String _packageName, final int _iconId) {
            packageName = _packageName;
            iconId = _iconId;
        }

        ResourceName(final ApplicationInfo aInfo, final int _iconId) {
            this(aInfo.packageName, _iconId);
        }

        ResourceName(final ComponentInfo cInfo, final int _iconId) {
            this(cInfo.applicationInfo.packageName, _iconId);
        }

        ResourceName(final ResolveInfo rInfo, final int _iconId) {
            this(rInfo.activityInfo.applicationInfo.packageName, _iconId);
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ResourceName that = (ResourceName) o;

            if (iconId != that.iconId) {
                return false;
            }
            return !(packageName != null ?
                    !packageName.equals(that.packageName) : that.packageName != null);

        }

        @Override
        public int hashCode() {
            int result;
            result = packageName.hashCode();
            result = 31 * result + iconId;
            return result;
        }

        @Override
        public String toString() {
            return "{ResourceName " + packageName + " / " + iconId + "}";
        }
    }

    private CharSequence getCachedString(final ResourceName name) {
        synchronized (sSync) {
            WeakReference<CharSequence> wr = sStringCache.get(name);
            if (wr != null) {   // we have the activity
                CharSequence cs = wr.get();
                if (cs != null) {
                    return cs;
                }
                // our entry has been purged
                sStringCache.remove(name);
            }
        }
        return null;
    }

    private void putCachedString(final ResourceName name, final CharSequence cs) {
        synchronized (sSync) {
            sStringCache.put(name, new WeakReference<CharSequence>(cs));
        }
    }

    @Override
    public CharSequence getText(final String packageName, final int resid,
            ApplicationInfo appInfo) {
        ResourceName name = new ResourceName(packageName, resid);
        CharSequence text = getCachedString(name);
        if (text != null) {
            return text;
        }
        if (appInfo == null) {
            try {
                appInfo = getApplicationInfo(packageName, 0);
            } catch (NameNotFoundException e) {
                return null;
            }
        }
        try {
            Resources r = getResourcesForApplication(appInfo);
            text = r.getText(resid);
            putCachedString(name, text);
            return text;
        } catch (NameNotFoundException e) {
            Log.w("PackageManager", "Failure retrieving resources for"
                    + appInfo.packageName);
        } catch (RuntimeException e) {
            // If an exception was thrown, fall through to return
            // default icon.
            Log.w("PackageManager", "Failure retrieving text 0x"
                    + Integer.toHexString(resid) + " in package "
                    + packageName, e);
        }
        return null;
    }

    @Override
    public XmlResourceParser getXml(final String packageName, final int resid,
            ApplicationInfo appInfo) {
        if (appInfo == null) {
            try {
                appInfo = getApplicationInfo(packageName, 0);
            } catch (NameNotFoundException e) {
                return null;
            }
        }
        try {
            Resources r = getResourcesForApplication(appInfo);
            return r.getXml(resid);
        } catch (RuntimeException e) {
            // If an exception was thrown, fall through to return
            // default icon.
            Log.w("PackageManager", "Failure retrieving xml 0x"
                    + Integer.toHexString(resid) + " in package "
                    + packageName, e);
        } catch (NameNotFoundException e) {
            Log.w("PackageManager", "Failure retrieving resources for "
                    + appInfo.packageName);
        }
        return null;
    }

    @Override
    public CharSequence getApplicationLabel(final ApplicationInfo info) {
        return info.loadLabel(this);
    }

    @Override
    public void installPackage(final Uri packageURI, final IPackageInstallObserver observer, final int flags,
            final String installerPackageName) {
        try {
            mPM.installPackage(packageURI, observer, flags, installerPackageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void installPackageWithVerification(final Uri packageURI, final IPackageInstallObserver observer,
            final int flags, final String installerPackageName, final Uri verificationURI,
            final ManifestDigest manifestDigest, final ContainerEncryptionParams encryptionParams) {
        // try {
        // mPM.installPackageWithVerification(packageURI, observer, flags, installerPackageName,
        // verificationURI, manifestDigest, encryptionParams);
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    @Override
    public void installPackageWithVerificationAndEncryption(final Uri packageURI,
            final IPackageInstallObserver observer, final int flags, final String installerPackageName,
            final VerificationParams verificationParams, final ContainerEncryptionParams encryptionParams) {
        // try {
        // mPM.installPackageWithVerificationAndEncryption(packageURI, observer, flags,
        // installerPackageName, verificationParams, encryptionParams);
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    // @Override
    // public int installExistingPackage(final String packageName)
    // throws NameNotFoundException {
    // try {
    // int res = mPM.installExistingPackage(packageName);
    // if (res == INSTALL_FAILED_INVALID_URI) {
    // throw new NameNotFoundException("Package " + packageName + " doesn't exist");
    // }
    // return res;
    // } catch (RemoteException e) {
    // // Should never happen!
    // throw new NameNotFoundException("Package " + packageName + " doesn't exist");
    // }
    // }

    @Override
    public void verifyPendingInstall(final int id, final int response) {
        try {
            mPM.verifyPendingInstall(id, response);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void extendVerificationTimeout(final int id, final int verificationCodeAtTimeout,
            final long millisecondsToDelay) {
        // try {
        // mPM.extendVerificationTimeout(id, verificationCodeAtTimeout, millisecondsToDelay);
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    @Override
    public void setInstallerPackageName(final String targetPackage,
            final String installerPackageName) {
        try {
            mPM.setInstallerPackageName(targetPackage, installerPackageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void movePackage(final String packageName, final IPackageMoveObserver observer, final int flags) {
        try {
            mPM.movePackage(packageName, observer, flags);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public String getInstallerPackageName(final String packageName) {
        try {
            return mPM.getInstallerPackageName(packageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
        return null;
    }

    @Override
    public void deletePackage(final String packageName, final IPackageDeleteObserver observer, final int flags) {
        try {
            mPM.deletePackage(packageName, observer, flags);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }
    @Override
    public void clearApplicationUserData(final String packageName,
            final IPackageDataObserver observer) {
        // try {
        // mPM.clearApplicationUserData(packageName, observer, mContext.getUserId());
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }
    @Override
    public void deleteApplicationCacheFiles(final String packageName,
            final IPackageDataObserver observer) {
        try {
            mPM.deleteApplicationCacheFiles(packageName, observer);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }
    @Override
    public void freeStorageAndNotify(final long idealStorageSize, final IPackageDataObserver observer) {
        try {
            mPM.freeStorageAndNotify(idealStorageSize, observer);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void freeStorage(final long freeStorageSize, final IntentSender pi) {
        try {
            mPM.freeStorage(freeStorageSize, pi);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void getPackageSizeInfo(final String packageName, final int userHandle,
            final IPackageStatsObserver observer) {
        // try {
        // mPM.getPackageSizeInfo(packageName, userHandle, observer);
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }
    @Override
    public void addPackageToPreferred(final String packageName) {
        try {
            mPM.addPackageToPreferred(packageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void removePackageFromPreferred(final String packageName) {
        try {
            mPM.removePackageFromPreferred(packageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public List<PackageInfo> getPreferredPackages(final int flags) {
        try {
            return mPM.getPreferredPackages(flags);
        } catch (RemoteException e) {
            // Should never happen!
        }
        return new ArrayList<PackageInfo>();
    }

    @Override
    public void addPreferredActivity(final IntentFilter filter,
            final int match, final ComponentName[] set, final ComponentName activity) {
        // try {
        // mPM.addPreferredActivity(filter, match, set, activity, mContext.getUserId());
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    @Override
    public void addPreferredActivity(final IntentFilter filter, final int match,
            final ComponentName[] set, final ComponentName activity, final int userId) {
        // try {
        // mPM.addPreferredActivity(filter, match, set, activity, userId);
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    @Override
    public void replacePreferredActivity(final IntentFilter filter,
            final int match, final ComponentName[] set, final ComponentName activity) {
        try {
            mPM.replacePreferredActivity(filter, match, set, activity);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public void clearPackagePreferredActivities(final String packageName) {
        try {
            mPM.clearPackagePreferredActivities(packageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
    }

    @Override
    public int getPreferredActivities(final List<IntentFilter> outFilters,
            final List<ComponentName> outActivities, final String packageName) {
        try {
            return mPM.getPreferredActivities(outFilters, outActivities, packageName);
        } catch (RemoteException e) {
            // Should never happen!
        }
        return 0;
    }

    @Override
    public void setComponentEnabledSetting(final ComponentName componentName,
            final int newState, final int flags) {
        // try {
        // mPM.setComponentEnabledSetting(componentName, newState, flags, mContext.getUserId());
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    @Override
    public int getComponentEnabledSetting(final ComponentName componentName) {
        // try {
        // return mPM.getComponentEnabledSetting(componentName, mContext.getUserId());
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
        return PackageManager.COMPONENT_ENABLED_STATE_DEFAULT;
    }

    @Override
    public void setApplicationEnabledSetting(final String packageName,
            final int newState, final int flags) {
        // try {
        // mPM.setApplicationEnabledSetting(packageName, newState, flags, mContext.getUserId());
        // } catch (RemoteException e) {
        // // Should never happen!
        // }
    }

    // @Override
    // public int getApplicationEnabledSetting(final String packageName) {
    // try {
    // return mPM.getApplicationEnabledSetting(packageName, mContext.getUserId());
    // } catch (RemoteException e) {
    // // Should never happen!
    // }
    // return PackageManager.COMPONENT_ENABLED_STATE_DEFAULT;
    // }

    /**
     * @hide
     */
    @Override
    public VerifierDeviceIdentity getVerifierDeviceIdentity() {
        try {
            return mPM.getVerifierDeviceIdentity();
        } catch (RemoteException e) {
            // Should never happen!
        }
        return null;
    }

    // private final ContextImpl mContext;
    private final IPackageManager mPM = null;

    private static final Object sSync = new Object();
    private static HashMap<ResourceName, WeakReference<Drawable.ConstantState>> sIconCache
    = new HashMap<ResourceName, WeakReference<Drawable.ConstantState>>();
    private static HashMap<ResourceName, WeakReference<CharSequence>> sStringCache
    = new HashMap<ResourceName, WeakReference<CharSequence>>();

    @Override
    public ApplicationInfo getApplicationInfo(final String packageName, final int flags) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ActivityInfo getActivityInfo(final ComponentName component, final int flags) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ActivityInfo getReceiverInfo(final ComponentName component, final int flags) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ServiceInfo getServiceInfo(final ComponentName component, final int flags) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProviderInfo getProviderInfo(final ComponentName component, final int flags) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<PackageInfo> getInstalledPackages(final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<PackageInfo> getInstalledPackages(final int flags, final int userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void grantPermission(final String packageName, final String permissionName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void revokePermission(final String packageName, final String permissionName) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<ApplicationInfo> getInstalledApplications(final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResolveInfo resolveActivity(final Intent intent, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResolveInfo resolveActivityAsUser(final Intent intent, final int flags, final int userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryIntentActivities(final Intent intent, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryIntentActivitiesAsUser(final Intent intent, final int flags, final int userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryIntentActivityOptions(final ComponentName caller, final Intent[] specifics,
            final Intent intent, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryBroadcastReceivers(final Intent intent, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryBroadcastReceivers(final Intent intent, final int flags, final int userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResolveInfo resolveService(final Intent intent, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryIntentServices(final Intent intent, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ResolveInfo> queryIntentServicesAsUser(final Intent intent, final int flags, final int userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProviderInfo resolveContentProvider(final String name, final int flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Drawable getDrawable(final String packageName, final int resid, final ApplicationInfo appInfo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Drawable getDefaultActivityIcon() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Resources getResourcesForApplication(final ApplicationInfo app) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Resources getResourcesForApplicationAsUser(final String appPackageName, final int userId)
            throws NameNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int installExistingPackage(final String packageName) throws NameNotFoundException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getApplicationEnabledSetting(final String packageName) {
        // TODO Auto-generated method stub
        return 0;
    }
}
